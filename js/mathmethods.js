let base10Log = math.log(1000000, 10);
document.getElementById("base10Log").innerHTML = base10Log;
let base2Log = math.log(64, 2);
document.getElementById("base2Log").innerHTML = base2Log;
let base4Log = math.log(16384, 4);
document.getElementById("base4Log").innerHTML = base4Log;
let squareRoot96 = math.sqrt(96);
document.getElementById("squareRoot96").innerHTML = squareRoot96;
let squareRoot64 = math.sqrt(-64);
document.getElementById("squareRoot64").innerHTML = squareRoot64;
let simpleExpress = "Don't understand how to complete this one";
document.getElementById("simpleExpress").innerHTML = simpleExpress;

let cosine = math.cos(45);
document.getElementById("cosine").innerHTML = cosine;

